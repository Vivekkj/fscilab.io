# Website files of FSCI 
[![pipeline status](https://gitlab.com/fsci/fsci.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/fsci/fsci.gitlab.io/-/commits/master)
### Build Requirements:

- Hugo
- Git

### USAGE:

It's good to have basic knowledge of Hugo, git & markdown. Don't worry, It's easy to learn them :-)

You can get started with hugo from the [official hugo website](https://gohugo.io) or watch this [hugo tutorial playlist](https://www.youtube.com/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3) on youtube. It will help you to understand hugo easily.

#### **Create a new blog post:**

- Clone this repo & `cd` into it

- Run `hugo new blog/title-of-the-new-statement.md`

- A new markdown file will be created under `content/statements/` folder, Use your favourite text editor to edit it. 

- To see website preview, run `hugo server -D`

- Open `localhost:1313` in your browser

- When you finish drafting the statement, To make it live, set `draft: true` & add relevant `tags` in your file.

- Commit your changes & git push them, That's it :-) 
