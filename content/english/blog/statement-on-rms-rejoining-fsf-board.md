---
title: "FSCI Position Statement on the Controversies Surrounding Richard M Stallman's Reinstatement to the Board of FSF"
date: 2021-04-13T22:22:49+05:30
author: "false"
tags: [RMS, FSF]
discussionlink: https://codema.in/d/Xdi7EPS9/statement-on-richard-stallman-rejoining-the-fsf-board
draft: false
---

FSCI is a community of contributors to the free software movement from India.

At FSCI we value software freedom because it is ultimately about user freedom. And we believe that inherent in this value is the spirit of inclusion and diversity. Everyone should have the freedom to use software, especially those who need it the most.

We believe that being sensitive to the context, background, and individuality of each individual who participates or wishes to participate in free software movement is important. Only such sensitivity will help us be a thriving community that brings valuable change to the world.

We believe that as a community we should encourage in everyone conduct that harbors respect, trust, and empathy. Especially so in people we look up to.

We believe that human beings always have room for learning and growth. In other words, human beings are imperfect. But we do not take that as an excuse to encourage inappropriate conduct.

We also believe that there are several ways to deal with inappropriate conduct in the community. A one-size-fits-all approach would be ignoring the individuality of human beings which goes against our values.

Richard Stallman's reinstatement to the board of Free Software Foundation has been met with several reactions. There are people who strongly oppose this move, people who strongly support it, and people all the way in between. As a community of passionate contributors, FSCI also had intense discussions about all of this. It has led to learning, growth, exhaustion, and pain among our community members.

In this situation, we believe that it is in the best interest of our community to not make a judgement about Richard M Stallman or Free Software Foundation, but to take valuable lessons from this whole situation to make our community better.

We have a code of conduct and we should constantly improve the same.

We need to proactively reach out to people from underrepresented groups and welcome them into our community.

We need to keep strengthening free software.